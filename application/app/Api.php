<?php
/**
 * Created by PhpStorm.
 * User: MD. RABIUL ISLAM
 * Date: 16/02/2018
 * Time: 01:21
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Api extends Model
{
    protected $table = 'sys_device_access';
}