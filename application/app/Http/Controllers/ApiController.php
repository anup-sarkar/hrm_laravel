<?php

namespace App\Http\Controllers;

use App\Api;
use App\Attendance;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DateTime;

date_default_timezone_set(app_config('Timezone'));

class ApiController extends Controller
{

	public function attendance(Request $request) {
        $contentType = isset($_SERVER["CONTENT_TYPE"]) ? trim($_SERVER["CONTENT_TYPE"]) : '';
        
        if(strcasecmp($contentType, 'application/json') != 0){
            return response([
                'status' => FALSE,
                'message' => 'Content type must be: application/json'
            ], 400);
        }

        $content = trim(file_get_contents("php://input"));
        $decoded = json_decode($content);

        if(!$decoded) {
            return response([
                'status' => FALSE,
                'message' => 'Received content contained invalid JSON!'
            ], 404);
        }

        $log_id = DB::table('sys_app_log')->insertGetId(
		['request' => $content]
	);

        if(!isset($decoded->username) || !isset($decoded->password) || !isset($decoded->device) || !isset($decoded->data)) {
            return response([
                'status' => FALSE,
                'message' => 'Content type must have username or password or device or data keys'
            ], 400);
        }

        $username  = filter_var($decoded->username, FILTER_SANITIZE_STRING);
        $password  = filter_var($decoded->password, FILTER_SANITIZE_STRING);
        $password  = md5($password);
        $device_id = filter_var($decoded->device, FILTER_SANITIZE_NUMBER_INT);
        $logs      = $decoded->data;

        $user = Api::where("username", "=", $username)->where("password", "=", $password)->first();
        if(!$user) {
            return response([
                'status' => FALSE,
                'message' => 'No user were found'
            ], 404);
        }

        if(empty($logs)) {
            return response([
                'status'  => FALSE,
                'message' => 'No logs'
            ], 400);
        }
        
        $rec = [];
        $rej = [];
        foreach($logs as $log) {
            if(!isset($log->id) || !isset($log->id)) {
                continue;
            }
                        
            $dev_time = new DateTime($log->time); //Device time

            $employee = Employee::where('employee_code', $log->id)->first();
            if(!$employee) {
            	if(!in_array($log->id, $rej))
            	     array_push($rej, $log->id);
            	continue;
            }
            
            if(!in_array($log->id, $rec))
	    	array_push($rec, $log->id);
	    
            $attendance = Attendance::where('date', $dev_time->format('Y-m-d'))->where('emp_id', $employee->id)->first();
            if ($attendance) {
                if ($attendance->clock_status == 'Clock Out' || $attendance->clock_status == '') {
                    $attendance->clock_status='Clock In';
                    $attendance->clock_in_optional=$dev_time->format('g:i A');
                    $attendance->save();
                } else {
                    $clock_out = $dev_time->format('g:i A');
                    $office_out_time = app_config('OfficeOutTime');
                    $office_in_time = app_config('OfficeInTime');

                    $office_hour = (strtotime($office_out_time) - strtotime($office_in_time)) / 60;

                    if($attendance->clock_in_optional==''){
                        $clock_in = $attendance->clock_in;

                        $late_1 = strtotime($office_in_time);
                        $late_2 = strtotime($clock_in);
                        $late = ($late_2 - $late_1);

                        if ($late < 0) {
                            $late = 0;
                        }
                        $late = $late / 60;


                        $early_leave_1 = strtotime($clock_out);
                        $early_leave_2 = strtotime($office_out_time);
                        $early_leave = ($early_leave_2 - $early_leave_1);

                        if ($early_leave < 0) {
                            $early_leave = 0;
                        }
                        $early_leave = $early_leave / 60;

                        $total = $office_hour - $late - $early_leave;
                        $attendance->late = $late;
                    } else{

                        $clock_in=$attendance->clock_in_optional;
                        $early_leave_1 = strtotime($clock_out);
                        $early_leave_2 = strtotime($office_out_time);
                        $early_leave = ($early_leave_2 - $early_leave_1);

                        if ($early_leave < 0) {
                            $early_leave = 0;
                        }

                        $early_leave=$early_leave/60;
                        $total=$office_hour-($attendance->early_leaving+$attendance->late);
                    }

                    $attendance->clock_out = $clock_out;
                    $attendance->early_leaving = $early_leave;
                    $attendance->overtime= '0';
                    $attendance->total= $total;
                    $attendance->clock_status='Clock Out';
                    $attendance->save();
                }

            } else {
                $entry = new Attendance();
                $entry->emp_id = $employee->id;
                $entry->designation = $employee->designation;
                $entry->department = $employee->department;
                $entry->date = $dev_time->format('Y-m-d');
                $entry->clock_in = $dev_time->format('g:i A');
                $entry->status = 'Present';
                $entry->clock_status = 'Clock In';
                $entry->save();
            }

        }
        
        sort($rec);
        sort($rej);
        
        DB::table('sys_app_log')
            ->where('id', $log_id)
            ->update(['received' => json_encode($rec), 'rejected' => json_encode($rej)]);
        
        return response([
            'status'  => TRUE,
            'message' => 'Success'
        ], 200);
    }

}