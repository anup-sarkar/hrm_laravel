@extends('master')

{{--External Style Section--}}
@section('style')
    {!! Html::style("assets/libs/data-table/datatables.min.css") !!}
    {!! Html::style("assets/libs/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css") !!}
@endsection


@section('content')




  <section class="wrapper-bottom-sec">
        <div class="p-30">
            <h2 class="page-title">{{language_data('Attendance')}}</h2>
        </div>



        <div class="p-30 p-t-none p-b-none">
            @include('notification.notify')
            <div class="row">

                <div class="col-lg-12">
                    <div class="panel">
                        <div class="panel-heading">
                            <h3 class="panel-title">Daily Attandence Report</h3>
                        </div>
                        <div class="panel-body">
                            


  <form class="" role="form" method="post" action="{{url('attendance/search-single')}}">

                               <table>
                                   <tr>
                                       <td rowspan="5">
                                           
                                            <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group">
                                            <label for="el2">{{language_data('Date From')}}</label>
                                            <input type="text" id="date" class="form-control datePicker" required="" name="date" value="{{get_date_format($date)}}">
                                        </div>
                                    </div>

                                </div>
                                  
                                       </td>
                                       <td>  <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <button style="margin-top: 12px;margin-left: 10px" type="submit" class="btn btn-success pull-left"><i class="fa fa-search"></i> {{language_data('Search')}}</button></td>
                                   </tr>
                               </table>
</form>



<table style="width:100%;" cellpadding="0" cellspacing="0" class="table table-border">
            <tbody><tr>
                <td colspan="8">
                    <div style="float:left;">
                        <strong>Department:</strong>
                        Accounts
                    </div>
                    <div style="float:right;">
                        <strong>Date:</strong>
                        03/04/2018
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="8" align="center" valign="middle"><h2><u>Daily Attendance Report</u></h2></td>
            </tr>
            <tr align="center" valign="middle">
                <th width="3%">SL</th>
                <th width="14%">Total Employee</th>
                <th width="16%">Present Employee</th>
                <th width="15%">Late Attendance</th>
                <th width="10%">Absentee</th>
                <th width="17%">On Leave/ Day Off</th>
                <th width="25%" colspan="2">Employee On Leave/Day Off</th>
            </tr>
            <tr align="center" valign="middle">
                <td>01</td>
                <td>20</td>
                <td>15</td>
                <td>8</td>
                <td>2</td>
                <td>3</td>
                <td colspan="2">2</td>
            </tr>
            <tr>
                <td colspan="8" align="center" valign="middle"><h2><u>Status of Late Attendance &amp; Absentees</u></h2></td>
            </tr>
            <tr align="center" valign="middle">
                <th width="3%">SL</th>
                <th width="5%">ID</th>
                <th width="18%">Name Of Employee</th>
                <th width="15%">Designation</th>
                <th width="24%">Time Of Attendance for Late Attendee</th>
                <th width="10%">Absentee</th>
                <th width="15%">Weather Informed</th>
                <th width="10%">Remark</th>
            </tr>
            <tr align="center" valign="middle">
                <td>01</td>
                <td>10000</td>
                <td>Rabiul Islam</td>
                <td>IT Officer</td>
                <td>9.30am</td>
                <td>No</td>
                <td>Yes</td>
                <td>&nbsp;</td>
            </tr>
            <tr align="center" valign="middle">
                <td>02</td>
                <td>10000</td>
                <td>Himel Ali</td>
                <td>IT Manager</td>
                <td>9.30am</td>
                <td>No</td>
                <td>Yes</td>
                <td>&nbsp;</td>
            </tr>
        </tbody></table>







 





                        </div>
                    </div>
                </div>

            </div>

           

        </div>




    </section>




<div class="modal fade" id="myModal">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

         
                <div class="modal-header">
                    <h4 class="modal-title">Search Options</h4>

                </div>

            <!-- Modal body -->
                <div class="modal-body">
 
                   
 

                </div>
         
            <!-- Modal footer -->
                <div class="modal-footer">

                    <div class="col-md-12">
                        <br />
                        <br />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>
           
        </div>
    </div>
</div>




<div class="modal fade" id="myModal2">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">

         
                <div class="modal-header">
                    <h4 class="modal-title">Search Options</h4>

                </div>

            <!-- Modal body -->
                <div class="modal-body">
 
                   
 

                </div>
         
            <!-- Modal footer -->
                <div class="modal-footer">

                    <div class="col-md-12">
                        <br />
                        <br />
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Search</button>
                    </div>
                </div>
           
        </div>
    </div>
</div>


@endsection

{{--External Style Section--}}
@section('script')
    {!! Html::script("assets/libs/data-table/datatables.min.js")!!}
    {!! Html::script("assets/libs/handlebars/handlebars.runtime.min.js")!!}
    {!! Html::script("assets/libs/moment/moment.min.js")!!}
    {!! Html::script("assets/libs/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js")!!}
    {!! Html::script("assets/js/form-elements-page.js")!!}
    {!! Html::script("assets/js/bootbox.min.js")!!}
    <script>
        $(document).ready(function () {

            /*For DataTable*/
            $('.data-table').DataTable();


            /*Linked Date*/
 
                $('#date').data("DateTimePicker").minDate(e.date);
        

            $("#date_to").on("dp.change", function (e) {
                $('#date_from').data("DateTimePicker").maxDate(e.date);
            });


            /*For Designation Loading*/
            $("#department_id").change(function () {
                var id = $(this).val();
                var _url = $("#_url").val();
                var dataString = 'dep_id=' + id;
                $.ajax
                ({
                    type: "POST",
                    url: _url + '/attendance/get-designation',
                    data: dataString,
                    cache: false,
                    success: function ( data ) {
                        $("#designation").html( data).removeAttr('disabled').selectpicker('refresh');
                    }
                });
            });


            /*For Delete Attendance*/

            $(".cdelete").click(function (e) {
                e.preventDefault();
                var id = this.id;
                bootbox.confirm("Are you sure?", function (result) {
                    if (result) {
                        var _url = $("#_url").val();
                        window.location.href = _url + "/attendance/delete-attendance/" + id;
                    }
                });
            });



            /*Set Overtime*/
            $(".setOvertime").click(function (e) {
                e.preventDefault();

                var id = this.id;
                var _url = $("#_url").val();
                var  overTimeVal= $(this).data('overtime-val');
                var redirectURL=window.location.href;

                bootbox.prompt({
                    title: "Set Overtime (In Hour)?",
                    value: overTimeVal,
                    callback: function(result) {
                        var dataString = 'attend_id=' + id+'&overTimeValue='+result;
                        $.ajax
                        ({
                            type: "POST",
                            url: _url + '/attendance/set-overtime',
                            data: dataString,
                            cache: false,
                            success: function ( data ) {
                                if(data=='success'){
                                    window.location=redirectURL;
                                }else{
                                    alert('Please Try Again');
                                }

                            }
                        });
                    }
                });
            });





        });
    </script>


@endsection
